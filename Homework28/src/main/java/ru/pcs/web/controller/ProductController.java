package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.models.Product;
import ru.pcs.web.repositories.ProductRepository;

@Controller
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @PostMapping("/product")
    public String addProduct(@RequestParam("description") String description,
                             @RequestParam("price") Double price,
                             @RequestParam("number") Integer number) {
        Product product = Product.builder()
                .description(description)
                .price(price)
                .number(number)
                .build();
        productRepository.save(product);
        return "redirect:/product_add.html";
    }
}
