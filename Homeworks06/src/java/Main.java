import java.util.Arrays;

public class Main {
    public static int getIndex(int[] array, int number) {
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (number == array[i]) {
                index = i;
                return index;
            }
        }
        return index;
    }

    public static void trailingZeros(int[] array) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j] != 0) {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                        break;
                    }
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

    public static void main(String[] args) {
        int[] a = {0, 1, 2, 5, 99, 198, 146};
        System.out.println(getIndex(a, 198));
        System.out.println(getIndex(a, 106));
        int[] zero = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        trailingZeros(zero);
    }
}
