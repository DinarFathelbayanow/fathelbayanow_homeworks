public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        User byId = usersRepository.findById(6);
        byId.setName("Артур");
        byId.setAge(29);
        byId.setWorker(false);
        usersRepository.update(byId);
    }
}
