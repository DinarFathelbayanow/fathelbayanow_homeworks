import java.io.*;

public class UsersRepositoryFileImpl implements UsersRepository {
    private final String fileName;
    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }
    @Override
    public User findById(int id) {
        try (Reader reader = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int userId = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                if (id == userId) {
                    return new User(userId, name, age, isWorker);
                }
                line = bufferedReader.readLine();
            }
            return null;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User update(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        try ( BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int userId = Integer.parseInt(parts[0]);
                if (user.getId() == userId) {
                    stringBuilder.append(user.getId()).append("|")
                            .append(user.getName())
                            .append("|")
                            .append(user.getAge())
                            .append("|")
                            .append(user.isWorker())
                            .append("\n");
                } else {
                    stringBuilder.append(line)
                            .append("\n");
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("users.txt"))) {
            writer.write(stringBuilder.toString());
            return user;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
