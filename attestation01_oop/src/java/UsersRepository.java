public interface UsersRepository {
    User findById(int id);
    User update(User user);
}
