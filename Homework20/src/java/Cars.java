import java.util.Objects;

public class Cars {
    private final String id;
    private final String model;
    private final String colour;
    private final int mileage;
    private final int price;

    public Cars(String id, String model, String colour, int mileage, int price) {
        this.id = id;
        this.model = model;
        this.colour = colour;
        this.mileage = mileage;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public String getColour() {
        return colour;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cars cars = (Cars) o;
        return mileage == cars.mileage && price == cars.price && Objects.equals(id, cars.id) && Objects.equals(model, cars.model) && Objects.equals(colour, cars.colour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, model, colour, mileage, price);
    }

    @Override
    public String toString() {
        return "Cars{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", colour='" + colour + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }

}
