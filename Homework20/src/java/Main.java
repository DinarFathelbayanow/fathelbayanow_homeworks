public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepository("Cars.txt");
        System.out.println("Номера всех автомобилей, имеющих черный цвет или нулевой пробег: " + carsRepository.displayNumbersCar());
        System.out.print("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс = " + carsRepository.displayUniqueModels() + "\n");
        System.out.println("Вывести цвет автомобиля с минимальной стоимостью - " + carsRepository.displayColorMinPrice());
        System.out.println("Среднюю стоимость Camry - " + carsRepository.displayAveragePriseModel());
    }
}
