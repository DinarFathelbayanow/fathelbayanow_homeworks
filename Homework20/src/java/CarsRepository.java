import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepository {
    private final String fileName;

    public CarsRepository(String fileName) {
        this.fileName = fileName;
    }

    private final static Function<String, Cars> carsMapFunction = line -> {
        String[] parts = line.split("\\|");
        String number = parts[0];
        String model = parts[1];
        String colour = parts[2];
        int mileage = Integer.parseInt(parts[3]);
        int price = Integer.parseInt(parts[4]);
        return new Cars(number, model, colour, mileage, price);
    };

    private List<Cars> findAll() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(carsMapFunction).collect(Collectors.toList());

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<String> displayNumbersCar() {
        return findAll().stream()
                .filter((text) -> text.getColour().equals("чёрный") || text.getMileage() == 0)
                .map(Cars::getId).collect(Collectors.toList());
    }

    public long displayUniqueModels() {
        return findAll().stream().distinct()
                .filter(price -> price.getPrice() >= 700_000 && price.getPrice() <= 800_000)
                .map(Cars::getModel)
                .count();
    }

    public Optional<String> displayColorMinPrice() {
        return findAll().stream().
                min(Comparator.comparingInt(Cars::getPrice)).
                map(Cars::getColour);
    }

    public double displayAveragePriseModel() {
        return findAll().stream().
                filter(model -> model.getModel().equals("Camry"))
                .collect(Collectors.averagingInt(Cars::getPrice));
    }
}
