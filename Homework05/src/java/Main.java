import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
     min();
    }
    public static void min (){
        Scanner scanner = new Scanner(System.in);
        int min = Integer.MAX_VALUE;
        int value = scanner.nextInt();
        while (value != -1) {
            while (value != 0) {
                int last = value % 10;
                if (min > last) {
                    min = last;
                }
                value = value / 10;
            }
            value = scanner.nextInt();
        }
        System.out.println("минимальная цифра = " + min);
    }
}
