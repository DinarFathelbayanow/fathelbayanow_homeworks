
public class Main {
    public static void main(String[] args) {
        Logger logger = Logger.getInstance();
        logger.log("Hello, world!");
        logger.log("Программистами рождаются или становятся?");
        logger.log("Становятся. В результате долгого и упорного труда.");
        logger.log("Учиться, учиться и еще раз учиться!!!");
    }
}
