import java.util.Arrays;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        int[] result = new int[array.length];
        int count = 0;
        for (int j : array) {
            if (condition.isOk(j)) {
                result[count++] = j;
            }
        }
        return Arrays.copyOfRange(result, 0, count);
    }
}
