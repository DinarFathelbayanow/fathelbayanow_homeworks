import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = {145, 212, 325, 717, 105, 176, 2006, 1995, 302, 1563, -202, -111};
        int[] filter1 = Sequence.filter(array, number -> number % 2 == 0);
        System.out.println("проверка на четность элемента " + Arrays.toString(filter1));
        ByCondition byCondition = number -> {
            int sum = 0;
            while (number != 0) {
                int last = number % 10;
                sum = sum + last;
                number = number / 10;
            }
            return sum % 2 == 0;
        };
        int[] filter2 = Sequence.filter(array, byCondition);
        System.out.println("проверка, является ли сумма цифр элемента четным числом. " + Arrays.toString(filter2));
    }
}
