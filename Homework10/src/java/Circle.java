public class Circle extends Ellipse implements Moves {
    public Circle(int x, int y, double radius1) {
        super(x, y, radius1, radius1);
    }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public int getY() {
        return super.getY();
    }

    @Override
    public double getPerimeter() {
        return 2 * radius1 * PI;
    }

    @Override
    public void movesFigure(int a, int b) {
        this.x += a;
        this.y += a;
        System.out.printf("Координата Х сдвинута на точку %d %n", this.x);
        System.out.printf("Координата Y сдвинута на точку %d %n", this.y);
    }
}
