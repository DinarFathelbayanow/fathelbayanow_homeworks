public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(0, 0, 10);
        Square square = new Square(0, 0,5);
        Moves[] moves = new Moves[2];
        moves[0] = circle;
        moves[1] = square;
        for (Moves move : moves) {
            move.movesFigure(10, 10);
        }
    }
}
