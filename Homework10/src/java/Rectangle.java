public class Rectangle extends Figure{
    protected double a;
    private double b;

    public Rectangle(int x, int y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        System.out.print("Площадь прямоугольника = ");
        return 2 * (a + b);
    }

}
