public class Ellipse extends Figure {
    protected double radius1;
    protected double radius2;
    protected static final double PI = 3.14;

    public Ellipse(int x, int y, double radius1, double radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getPerimeter() {
        System.out.print("Площадь эллипса = ");
        return 4 * (((PI * radius1 * radius2) + ((radius1 - radius2) * (radius1 - radius2))) / (radius1 + radius2));
    }
}


