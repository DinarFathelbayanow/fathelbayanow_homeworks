public class Square extends Rectangle implements Moves {
    public Square(int x, int y, double a) {
        super(x, y, a, a);
    }

    @Override
    public double getPerimeter() {
        return 4 * a;
    }

    @Override
    public void movesFigure(int a, int b) {
        this.x += a;
        this.y += a;
        System.out.printf("Координата Х сдвинута на точку %d %n", this.x);
        System.out.printf("Координата Y сдвинута на точку %d %n", this.y);
    }
}
