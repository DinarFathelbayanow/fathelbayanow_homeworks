import java.util.ArrayList;
import java.util.List;

public class Launch {
    public static void runThread(int threadsCount) {
        List<SumThread> list = new ArrayList<>();
        int countElement = Main.array.length / threadsCount;
        for (int i = 0; i < threadsCount; i++) {
            int start = i * countElement;
            int finish = countElement * (i + 1);
            SumThread sumThread = new SumThread(start, finish, i);
            sumThread.start();
            list.add(sumThread);
        }
        for (SumThread sumThread : list) {
            try {
                sumThread.join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
