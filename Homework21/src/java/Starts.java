public class Starts {
    public static void runThread(int threadsCount) {
        int countElement = Main.array.length / threadsCount;
        for (int i = 0; i < threadsCount; i++) {
            int start = i * countElement;
            int finish = countElement * (i + 1);
            SumThread sumThread = new SumThread(start, finish);
            sumThread.start();
            try {
                sumThread.join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
