import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {
    public static int[] array;
    public static int[] sums;

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int numbersCount = scanner.nextInt();
        int threadsCount = scanner.nextInt();
        array = new int[numbersCount];
        sums = new int[threadsCount];
        // заполняем случайными числами
        IntStream.range(0, array.length).forEach(i -> array[i] = random.nextInt(100));
        int realSum = Arrays.stream(array).sum();
        System.out.println("realSum: " + realSum);

        Launch.runThread(threadsCount);
        int byThreadSum = Arrays.stream(sums).sum();
        System.out.println("byThreadSum: " + byThreadSum);
    }

}
