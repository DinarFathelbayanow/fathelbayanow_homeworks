public class SumThread extends Thread {
    private final int from;
    private final int to;
    private final int numberThread;

    public SumThread(int from, int to, int numberThread) {
        this.from = from;
        this.to = to;
        this.numberThread = numberThread;
    }

    @Override
    public void run() {
        int sum = 0;
        for (int j = getFrom(); j < getTo(); j++) {
            sum += Main.array[j];
        }
        System.out.println("[" + getName() + "] finished, sum: " + sum);
        Main.sums[getNumberThread()] = sum;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public int getNumberThread() {
        return numberThread;
    }

}
