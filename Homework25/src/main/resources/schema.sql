create table product
(
    id          serial primary key,
    description varchar(25),
    price       double precision,
    number      integer
);
insert into product(description, price, number)
values ('Телевизор', 30000.0, 5),
       ('Мультиварка', 5000.0, 7),
       ('Ноутбук', 89990.0, 3),
       ('Утюг', 3500.0, 17),
       ('Микроволновка', 17000.0, 6),
       ('Кофеварка', 36000.0, 16),
       ('Робот-пылесос', 36000.0, 16),
       ('Холодильник', 60000.0, 20);


create table costumer
(
    id        serial primary key,
    firstName varchar(20),
    lastName  varchar(20)
);
insert into costumer(firstName, lastName)
values ('Максим', 'Петров'),
       ('Питер', 'Паркер'),
       ('Дмитрий', 'Шишкин'),
       ('Том', 'Холланд'),
       ('Алиса', 'Яковлева'),
       ('Андрей', 'Быков'),
       ('Анна', 'Зарипова'),
       ('Александра', 'Смирнова'),
       ('Тимофей', 'Ионов'),
       ('Ксения', 'Макарова');

create table booking
(
    product_id      integer,
    foreign key (product_id) references product (id),
    costumer_id     integer,
    foreign key (costumer_id) references costumer (id),
    data            date,
    number_of_goods int
);
insert into booking(product_id, costumer_id, data, number_of_goods)
values (1, 2, '2021-11-26', 1),
       (2, 1, '2021-07-05', 1),
       (4, 4, '2017-06-06', 2),
       (5, 3, '2021-01-05', 1),
       (3, 5, '2021-08-21', 3),
       (6, 8, '2021-02-15', 4),
       (7, 7, '2019-03-08', 2),
       (6, 7, '2021-06-02', 1),
       (8, 8, '2017-05-03', 2),
       (8, 9, '2018-11-11', 3),
       (7, 10, '2016-10-13', 5),
       (3, 2, '2019-10-25', 3),
       (5, 5, '2021-06-11', 2),
       (6, 8, '2021-11-11', 1);
-- все товары, и цены по убыванию, у которых количество купленных товаров больше 2
select p.description AS product,
       p.price,
       booking.number_of_goods
from booking
         join product p on booking.product_id = p.id
where booking.number_of_goods > 2
order by p.price DESC;

-- Имя и фамилия зазачкика, которые приобрелли товары в 2021 году больше 1 раза
select costumer.firstName,
       costumer.lastName,
       b.data,
       b.number_of_goods
from costumer
         join booking b on costumer.id = b.costumer_id
where data > '2021-01-01'
  and b.number_of_goods > 1;

-- вывести товар(ы), которые покупались больше 2 раз с 2020 года со стоимостью больше 50 т.р.
select  p.description,
        p.price
from product p join booking b on p.id = b.product_id
where b.number_of_goods > 2 and b.data < '2020.02.01' and p.price > 50000;

-- min стоимость товара, который покупался больше 1 раза
select min(price)
from product join booking b on product.id = b.product_id
where b.number_of_goods > 1

--найти все товары по количеству заказов, в которых участвуют
select p.id,
       p.description,
       p.price,
       p.number,
       b.number_of_goods AS ordersCount
from product p join booking b on p.id = b.product_id
order by b.number_of_goods;

