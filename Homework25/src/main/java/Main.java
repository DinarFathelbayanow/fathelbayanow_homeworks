import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/example",
                "postgres", "admin");
       ProductRepository productRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);
       Product product = Product.builder()
               .description("Блендер")
               .price(16000)
               .number(7)
               .build();
        productRepository.save(product);
        System.out.println(productRepository.findAll());
        System.out.println(productRepository.findAllByPrice(89990.0));
        System.out.println(productRepository.findAllByOrdersCount(1));
    }
}
