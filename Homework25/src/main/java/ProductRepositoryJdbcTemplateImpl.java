import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;

public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {
    private final JdbcTemplate jdbcTemplate;
    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    //language=SQL
    private static final String SQL_INSERT = "insert into product(description, price, number) values (?,?,?)";
    //language=SQL
    private static final String SQL_ORDERS_COUNT = "select p.id,\n" +
            "       p.description,\n" +
            "       p.price,\n" +
            "       p.number,\n" +
            "       b.number_of_goods AS ordersCount\n" +
            "from product p join booking b on p.id = b.product_id\n" +
            "order by b.number_of_goods";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from product order by id";

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        double price = row.getDouble("price");
        int number = row.getInt("number");
        return new Product(id, description, price, number);
    };

    private static final RowMapper<Product> ordersCountRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        double price = row.getDouble("price");
        int number = row.getInt("ordersCount");

        return new Product(id, description, price, number);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getNumber());
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return findAll().stream().filter(cost -> cost.getPrice() == price).
                collect(Collectors.toList());

    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_ORDERS_COUNT, ordersCountRowMapper)
                .stream()
                .filter(quantity -> quantity.getNumber() == ordersCount)
                .collect(Collectors.toList());
    }
}
