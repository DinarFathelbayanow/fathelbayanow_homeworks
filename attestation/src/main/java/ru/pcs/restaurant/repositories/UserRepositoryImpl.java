//package ru.pcs.restaurant.repositories;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Component;
//import ru.pcs.restaurant.models.User;
//
//import javax.sql.DataSource;
//import java.util.List;
//
//@Component
//public class UserRepositoryImpl implements UserRepository {
//    private final JdbcTemplate jdbcTemplate;
//
//    @Autowired
//    public UserRepositoryImpl(JdbcTemplate jdbcTemplate) {
//        this.jdbcTemplate = jdbcTemplate;
//    }
//
//
//    //language=SQL
//    private static final String SQL_INSERT = "insert into users(first_name, email, login, password) values (?,?,?,?)";
//    //language=SQL
//    private static final String SQL_SELECT_ALL = "select * from users order by id";
//    //language=SQL
//    private static final String SQL_DELETE_BY_ID = "delete from users where id = ?";
//    //language=SQL
//    private static final String SQL_SELECT_BY_ID = "select * from users where id = ?";
//
//    private static final RowMapper<User> userRowMapper = (row, rowNumber) -> {
//        Long id = row.getLong("id");
//        String first_name = row.getString("first_name");
//        String email = row.getString("email");
//        String login = row.getString("login");
//        String password = row.getString("password");
//
//        return new User(id, first_name, email, login, password);
//    };
//
//    @Override
//    public void save(User user) {
//        jdbcTemplate.update(SQL_INSERT, user.getFirst_name(), user.getEmail(), user.getLogin(), user.getPassword());
//    }
//
//    @Override
//    public List<User> findAll() {
//        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
//    }
//
//    @Override
//    public void delete(Long userId) {
//        jdbcTemplate.update(SQL_DELETE_BY_ID, userId);
//    }
//
//    @Override
//    public User findById(Long userId) {
//        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, userRowMapper, userId);
//    }
//}
