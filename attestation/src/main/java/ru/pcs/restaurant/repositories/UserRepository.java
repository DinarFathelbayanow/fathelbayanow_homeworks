package ru.pcs.restaurant.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.restaurant.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
