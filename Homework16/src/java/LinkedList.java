public class LinkedList <T>{
    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        // создаю новый узел
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);

        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public int size() {
        return size;
    }

    public T get(int index) {
        int count  = 0;
        Node<T> node = first;
        while (node!= null){
            if (count == index){
                return node.value;
            }
            node = node.next;
            count++;

        }
        return null;
    }

    public void remove(T t){
        if (first == null){
            return;
        }
        if (first.value == t){
            first = first.next;
            return;
        }
        Node<T> node = first;
        while (node.next != null){
            if (node.next.value == t){
                node.next = node.next.next;
                return;
            }
            node = node.next;
        }
    }

}
