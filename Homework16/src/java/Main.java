
public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < 16; i++) {
            numbers.add(i);
        }
        System.out.println(numbers);
        System.out.println("size = " + numbers.getSize());
        numbers.removeAt(2);
        numbers.removeAt(11);
        numbers.removeAt(12);
        System.out.println(numbers);
        System.out.println("size = " + numbers.getSize());

        LinkedList<Integer> linkedList = new LinkedList<>();
        for (int i = 0; i < 15; i++) {
            linkedList.add(i + 10);
            System.out.print(linkedList.get(i) + ", ");

        }
        System.out.println("\n" + linkedList.get(6));
        linkedList.remove(15);
        linkedList.remove(10);
    }

}
