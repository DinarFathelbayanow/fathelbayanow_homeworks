public class Main {
    public static void main(String[] args) {
       Figure figure = new Figure(4, 4);
       Ellipse ellipse = new Ellipse(5,5, 7,9);
       Circle circle = new Circle(6, 6, 5.5);
       Rectangle rectangle = new Rectangle(7,7, 4,5);
       Square square = new Square(4,3,6.5);
       Figure[] figures = new Figure[5];
       figures[0] = figure;
       figures[1] = ellipse;
       figures[2] = circle;
       figures[3] = rectangle;
       figures[4] = square;
        for (int i = 0; i < figures.length; i++) {
            System.out.println(figures[i].getPerimeter());
        }
    }
}
