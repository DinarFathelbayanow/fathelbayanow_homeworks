import java.util.*;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        Map<String, Integer> map = SplitText.countsWords(text);
        map.forEach((word, number) -> System.out.println("Слово: " + word + " количество раз = " + number));
        scanner.close();
    }
}
