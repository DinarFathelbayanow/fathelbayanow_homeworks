import java.util.*;

public class SplitText {

    public static Map<String, Integer> countsWords(String text) {
        List<String> list = new ArrayList<>(Arrays.asList(text.toLowerCase().split(" ")));
        Map<String, Integer> result = new HashMap<>();
        int count = 1;
        list.forEach(word -> result.merge(word, count, (k, v) -> v + count));
        return result;
    }
}
