import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void fillPeople(Person[] people) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < people.length; i++) {
            people[i] = new Person();
            System.out.println("Введите имя человека под индексом № " + i);
            people[i].setName(scanner.nextLine());
            System.out.println("Введите weight для  " + people[i].getName());
            people[i].setWeight(Double.parseDouble(scanner.nextLine()));
        }
    }

    public static void printPeople(Person[] people) {
        System.out.println(Arrays.toString(people));
    }

    public static void selectionSort(Person[] people) {
        for (int i = 0; i < people.length; i++) {
            double min = people[i].getWeight();
            int minIndex = i;
            for (int j = i + 1; j < people.length; j++) {
                if (people[j].getWeight() < min) {
                    min = people[j].getWeight();
                    minIndex = j;
                }
            }
            Person temp = people[i];
            people[i] = people[minIndex];
            people[minIndex] = temp;
        }
    }

    public static void main(String[] args) {
        Person[] people = new Person[10];
        fillPeople(people);
        printPeople(people);
        selectionSort(people);
        printPeople(people);
    }
}
