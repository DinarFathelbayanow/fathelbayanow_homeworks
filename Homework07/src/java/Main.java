import java.util.Arrays;

public class Main {
    public static void searchMin(int[] array) {
        int[] numbers = new int[201];
        // создаем массив от -100 до 100 и считаем сколько раз каждое число встречается в последовательности
        for (int i = 0; i < array.length; i++) {
            numbers[array[i] + 100]++;
        }

        int min = Integer.MAX_VALUE;
        int index = 0;
        // пробегаемся по массиву, ищем число которое встречается минимальное количетсво раз и его выводим.
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] != 0) {
                if (min > numbers[i]) {
                    min = numbers[i];
                    index = i;
                }
            }
        }
        System.out.printf("число, которе присутствует в последовательности минимальное количество раз %d", (index - 100));
    }

    public static void main(String[] args) {
        int[] array = new int[3_300_000_00];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * (200 + 1)) - 100;
        }
        searchMin(array);
    }
}
